This project was made for a web developer exam.
<br />
The app has 3 parts:
<br />
Part 1 is where you enter a number from the Fibonacci sequence, and the app returns the index number of the entered number. It will return an error message when the number entered is less than 4,000,000, is not a number, or is not a number from the Fibonacci sequence.
<br />
Part 2 is where you enter an array of numbers in the format of 1,2,3,4,5 in the first box, and either 1 or 2 for the second box. It will then sort the entered numbers depending on what the second input is. It will return an error when there is a value in the array that is not a number, the array has spaces, or when the sort method entered is not 1 or 2.
<br />
Part 3 is where a user can login and view a list of employees. If the logged in user is a super user, they can also add new employees, or update/archive the current employees. If the logged in user is not a super user, they can only view the list and not make any changes.
<br />
Default super user login and password is: admin@gmail.com admin
