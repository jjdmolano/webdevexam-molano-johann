import React, { useState, useEffect } from 'react'
import { UserProvider } from './UserContext'
import 'bootstrap/dist/css/bootstrap.min.css'
import NavBar from './components/NavBar'
import Part1 from './components/Part1'
import Part2 from './components/Part2'
import Part3 from './components/Part3'
import { Container, CardDeck, Card } from 'react-bootstrap'

export default function App() {
	const [user, setUser] = useState({
		id: null,
		access_level_id: null
	})

	const unsetUser = () => {
		localStorage.clear()
		setUser({
			id: null,
			access_level_id: null
		})
	}

	useEffect(() => {
		fetch('http://localhost:4000/api/users/details', {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
			.then(res => res.json())
			.then(data => {
				if (data._id) {
					setUser({
						id: data._id,
						access_level_id: data.access_level_id
					})
				} else {
					setUser({
						id: null,
						access_level_id: null
					})
				}
			})
	}, [user.id])

	return (
		<UserProvider value={{ user, setUser, unsetUser }}>
			<NavBar />
			<Container fluid className='App'>
				<h1>Web Dev Exam - Johann Molano</h1>
				<CardDeck className='firstPart'>
					<Part1 />
					<Part2 />
					{user.id === null ? (
						<Part3 />
					) : (
						<Card>
							<Card.Body>Already Logged In</Card.Body>
						</Card>
					)}
				</CardDeck>
			</Container>
		</UserProvider>
	)
}
