import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import Home from './components/Home'
import NotFound from './components/NotFound'
import Logout from './components/Logout'
import {BrowserRouter, Route, Switch} from 'react-router-dom'

const myComponent = (
    <>
        <BrowserRouter>
            <Switch>
                <Route exact path="/" component={App}/>
                <Route exact path="/home" component={Home}/>
                <Route exact path="/logout" component={Logout} />
                <Route component={NotFound}/>
            </Switch>
        </BrowserRouter>
    </>
)

const divRoot = document.getElementById('root')
ReactDOM.render(myComponent, divRoot);