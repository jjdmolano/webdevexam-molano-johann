import React from 'react'
import { Link } from 'react-router-dom'
import { Jumbotron, Button } from 'react-bootstrap'

export default function NotFound(){
	return (
		<Jumbotron>
            <h1>Woops! You've encountered an error.</h1>
            <p>Would you like to go back Home?</p>
            <Link to="/">
                <Button>Get Me Out!</Button>
            </Link>
		</Jumbotron>
	)
}