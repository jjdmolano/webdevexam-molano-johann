import React, { useState } from 'react'
import { Form, Button, Modal } from 'react-bootstrap'

export default function AddUser({ setUsers }) {
	const [firstName, setFirstName] = useState('')
	const [lastName, setLastName] = useState('')
	const [age, setAge] = useState('')
	const [birthDate, setBirthDate] = useState('')
	const [email, setEmail] = useState('')
	const [password, setPassword] = useState('')
	const [jobTitle, setJobTitle] = useState('')
	const [accessLevelId, setAccessLevelId] = useState('Regular User')
	const [show, setShow] = useState(false)
	const showModal = () => setShow(true)
	const closeModal = () => setShow(false)

	function addUser(e) {
		e.preventDefault()
		fetch('http://localhost:4000/api/users', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				age: age,
				birth_date: birthDate,
				email: email,
				password: password,
				job_title: jobTitle,
				access_level_id: accessLevelId
			})
		})
			.then(res => res.json())
			.then(data => {
				fetch('http://localhost:4000/api/users', {
					method: 'GET',
					headers: {
						Authorization: `Bearer ${localStorage.getItem('token')}`
					}
				})
					.then(res => res.json())
					.then(data => {
						setUsers(data)
						setShow(false)
					})
			})
	}

	return (
		<>
			<Button onClick={showModal}>Add</Button>
			<Modal show={show} onHide={closeModal} centered size='lg'>
				<Modal.Header closeButton>
					<Modal.Title>Add User</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<Form onSubmit={e => addUser(e)}>
						<Form.Group>
							<Form.Label>First Name:</Form.Label>
							<Form.Control
								type='text'
								value={firstName}
								onChange={e => setFirstName(e.target.value)}
								required
							/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Last Name:</Form.Label>
							<Form.Control
								type='text'
								value={lastName}
								onChange={e => setLastName(e.target.value)}
								required
							/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Age:</Form.Label>
							<Form.Control
								type='text'
								value={age}
								onChange={e => setAge(e.target.value)}
								required
							/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Birth Date:</Form.Label>
							<Form.Control
								type='text'
								value={birthDate}
								onChange={e => setBirthDate(e.target.value)}
								required
							/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Email:</Form.Label>
							<Form.Control
								type='email'
								value={email}
								onChange={e => setEmail(e.target.value)}
								required
							/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Password:</Form.Label>
							<Form.Control
								type='password'
								value={password}
								onChange={e => setPassword(e.target.value)}
								required
							/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Job Title:</Form.Label>
							<Form.Control
								type='test'
								value={jobTitle}
								onChange={e => setJobTitle(e.target.value)}
								required
							/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Access Level:</Form.Label>
							<Form.Control
								as='select'
								value={accessLevelId}
								onChange={e => setAccessLevelId(e.target.value)}
								required
							>
								<option>Regular User</option>
								<option>Super User</option>
							</Form.Control>
						</Form.Group>
						<br />
						<Button type='submit' block>
							Add
						</Button>
					</Form>
				</Modal.Body>
			</Modal>
		</>
	)
}
