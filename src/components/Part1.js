import React, { useState } from 'react'
import { Card, Form, Button } from 'react-bootstrap'

export default function Part1() {
    const [ number, setNumber ] = useState('')
    const [ message, setMessage ] = useState('')
    const arr = [ 0, 1 ]

    for (let i = 2; i < 100 + 1; i++) {
        arr.push(arr[i - 2] + arr[i - 1])
    }

	function fibonacci(e) {
        e.preventDefault()
		if (number === '') {
			return ''
		} else if (isNaN(number)) {
            setMessage('You have entered a value that is not a number.')
        } else if (number > 4000000) {
            const index = arr.findIndex(arrayNumber => arrayNumber === parseInt(number))
            if (index === -1) {
                setMessage('The number you entered is not part of the Fibonacci sequence.')
            } else {
                setMessage(`The index of that Fibonacci number is ${index}.`)
            }
		} else {
			setMessage('The number you entered is less than 4000000.')
		}
    }

	return (
        <Card>
            <Card.Title><h2>Part 1</h2></Card.Title>
            <Card.Body>
                <p>Please input the Fibonacci sequence number you would like to see the index of. (Minimum value is 4000000)</p>
                <Form onSubmit={e => fibonacci(e)}>
                    <Form.Control
                        type='text'
                        value={number}
                        placeholder="ex. 5702887"
                        onChange={e => setNumber(e.target.value)}
                    />
                    <br />
                    <Button type="submit" block>Check Index</Button>
                </Form>
                <br />
                <p>{message}</p>
            </Card.Body>
        </Card>
    )
}
