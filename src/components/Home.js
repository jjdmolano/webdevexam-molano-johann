import React, { useState, useEffect } from 'react'
import { UserProvider } from '../UserContext'
import UserList from './UserList'
import NavBar from './NavBar'
import { Container, Row, Col } from 'react-bootstrap'
import AddUser from './AddUser'

export default function Home() {
	const [users, setUsers] = useState('')
	const [user, setUser] = useState({
		id: null,
		access_level_id: null
	})

	const unsetUser = () => {
		localStorage.clear()
		setUser({
			id: null,
			access_level_id: null
		})
	}

	useEffect(() => {
		fetch('http://localhost:4000/api/users/details', {
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
			.then(res => res.json())
			.then(data => {
				if (data._id) {
					setUser({
						id: data._id,
						access_level_id: data.access_level_id
					})
				} else {
					setUser({
						id: null,
						access_level_id: null
					})
				}
			})
	}, [user.id])

	useEffect(() => {
		fetch('http://localhost:4000/api/users', {
			method: 'GET',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
			.then(res => res.json())
			.then(data => {
				setUsers(data)
			})
	}, [])

	return (
		<UserProvider value={{ user, setUser, unsetUser }}>
			<NavBar />
			<Container fluid>
                <Row>
				    {user.access_level_id === 'Super User' ? (
                        <Col md="auto">
					        <AddUser setUsers={setUsers} />
                        </Col>
				    ) : null}
                </Row>
				<UserList users={users} setUsers={setUsers} userAccess={user.access_level_id} />
			</Container>
		</UserProvider>
	)
}
