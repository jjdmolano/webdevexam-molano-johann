import React, { useState, useContext } from 'react'
import UserContext from '../UserContext'
import { Card, Form, Button } from 'react-bootstrap'
import { useHistory } from 'react-router-dom'

export default function Part3() {
    const { setUser } = useContext(UserContext)
    const [ email, setEmail ] = useState('')
    const [ password, setPassword ] = useState('')
    const history = useHistory()

    function authenticate(e) {
        e.preventDefault()

        fetch('http://localhost:4000/api/users/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(res => res.json())
        .then(data => {
            if(data.accessToken){
                localStorage.setItem('token', data.accessToken)
                fetch('http://localhost:4000/api/users/details', {
                    headers: {
                        Authorization: `Bearer ${data.accessToken}`
                    }
                })
                .then(res => res.json())
                .then(data => {
                    setUser({
                        id: data._id,
                        access_level_id: data.access_level_id
                    })
                    history.push("/home")
                })
            } else {
                history.push("/notfound")
            }
        })
    }

    return (
        <Card>
            <Card.Title><h2>Part 3</h2></Card.Title>
            <Card.Body>
                <h3>User Login</h3>
                <Form onSubmit={(e) => authenticate(e)}>
                    <Form.Group>
                        <Form.Label>Email:</Form.Label>
                        <Form.Control type="email" value={email} onChange={(e) => setEmail(e.target.value)} />
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Password:</Form.Label>
                        <Form.Control type="password" value={password} onChange={(e) => setPassword(e.target.value)} />
                    </Form.Group>
                    <br />
                    <Button type="submit" block>Login</Button>
                </Form>
            </Card.Body>
        </Card>
    )
}