import React from 'react'
import { Link } from 'react-router-dom'
import { Navbar, Nav } from 'react-bootstrap'
import Logout from './Logout'

export default function NavBar() {

	return (
		<Navbar expand='lg' collapseOnSelect>
            <Nav>
                <Link to='/' href='/'>
                    Home
                </Link>
                <Link to='/home' href='/home'>
                    List
                </Link>
                <Link to='/logout' href='/logout'>
                    Logout
                </Link>
            </Nav>
		</Navbar>
	)
}
