import React, { useContext } from 'react'
import UserContext from '../UserContext'
import { ListGroup, Row, Col } from 'react-bootstrap'
import UpdateUser from './UpdateUser'
import DeleteUser from './DeleteUser'

export default function UserList({ users, setUsers, userAccess }) {
	const { user } = useContext(UserContext)
	return (
		<ListGroup>
			<ListGroup.Item>
				<Row>
					<Col>
						<strong>First Name</strong>
					</Col>
					<Col>
						<strong>Last Name</strong>
					</Col>
					<Col>
						<strong>Age</strong>
					</Col>
					<Col>
						<strong>Birth Date</strong>
					</Col>
					<Col>
						<strong>Email</strong>
					</Col>
					<Col>
						<strong>Date Created</strong>
					</Col>
					<Col>
						<strong>Job Title</strong>
					</Col>
					<Col>
						<strong>Access Level</strong>
					</Col>
					{userAccess === 'Super User' ? (
						<>
                            <Col>
                                <strong>Is Archived</strong>
                            </Col>
							<Col>
								<strong>Edit User</strong>
							</Col>
							<Col>
								<strong>Delete User</strong>
							</Col>
						</>
					) : null}
				</Row>
			</ListGroup.Item>
			{users.length > 0 && user.id !== null ? (
				users.map(user => {
					const userDate = new Date(user.date_created).toLocaleString('en-US', {
						month: 'numeric',
						day: 'numeric',
						year: 'numeric'
					})

					return (
						<ListGroup.Item key={user._id}>
							<Row>
								<Col>{user.firstName}</Col>
								<Col>{user.lastName}</Col>
								<Col>{user.age}</Col>
								<Col>{user.birth_date}</Col>
								<Col>{user.email}</Col>
								<Col>{userDate}</Col>
								<Col>{user.job_title}</Col>
								<Col>{user.access_level_id}</Col>
								{userAccess === 'Super User' ? (
									<>
                                        <Col>
                                        {user.isActive ? 'Active' : 'Archived'}
                                        </Col>
										<Col>
											{user.firstName === 'Johann' &&
											user.lastName === 'Molano' ? null : (
												<UpdateUser
													users={users}
													userInfo={user}
													setUsers={setUsers}
												/>
											)}
										</Col>
										<Col>
											{user.firstName === 'Johann' &&
											user.lastName === 'Molano' ? null : (
												<DeleteUser
													users={users}
													userInfo={user}
													setUsers={setUsers}
												/>
											)}
										</Col>
									</>
								) : null}
							</Row>
						</ListGroup.Item>
					)
				})
			) : (
				<div>You need to be logged in to view this list.</div>
			)}
		</ListGroup>
	)
}
