import React, { useState } from 'react'
import { Card, Form, Button } from 'react-bootstrap'

export default function Part2() {
    const [ numbers, setNumbers ] = useState('')
    const [ numbersArray, setNumbersArray ] = useState([])
    const [ message, setMessage ] = useState('')
    const [ sortMethod, setSortMethod ] = useState('')
    const [ sortMessage, setSortMessage ] = useState('')

	function numberSorter(e) {
        e.preventDefault()
        setNumbersArray(numbers.split(',').map(number => parseInt(number)))
        const numberCheck = numbersArray.map(number => {
            return isNaN(number)
        })

        if (sortMethod === '1' && numberCheck.includes(true) === false && numbersArray.length >= 5) {
            numbersArray.sort((a, b) => a - b)
            setSortMessage(`Your sorted Array is ${numbersArray}`)
        } else if (sortMethod === '2' && numberCheck.includes(true) === false && numbersArray.length >= 5) {
            numbersArray.sort((a, b) => a - b).reverse()
            setSortMessage(`Your sorted Array is ${numbersArray}`)
        } else
            setSortMessage('This is not a valid sort option.')

        if (numberCheck.includes(true)) {
            setMessage('You have entered a value that is not a number.')
        } else {
            if (numbersArray.length < 5) {
                setMessage('You need at least 5 numbers to sort.')
            } else {
                setMessage('Numbers are valid!')
            }
        }
    }

	return (
        <Card>
            <Card.Title><h2>Part 2</h2></Card.Title>
            <Card.Body>
                <p>Please input an array of numbers and the sort method you want</p>
                <Form onSubmit={e => numberSorter(e)}>
                    <Form.Control
                        type='text'
                        value={numbers}
                        placeholder="Format: 1,2,3,4,5,etc."
                        onChange={e => setNumbers(e.target.value)}
                    />
                    <br />
                    <Form.Control
                        type='text'
                        value={sortMethod}
                        placeholder="ex. 1"
                        onChange={e => setSortMethod(e.target.value)}
                    />
                    <br />
                    <Button type="submit" block>Sort Array</Button>
                </Form>
                <br />
                <p>{message}</p>
                <p>{sortMessage}</p>
            </Card.Body>
        </Card>
	)
}
