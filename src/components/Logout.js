import { useState, useEffect } from 'react'
import { useHistory } from 'react-router-dom'

export default function Logout() {
    let history = useHistory()
	const [user, setUser] = useState({
		id: null,
		access_level_id: null
	})

	const unsetUser = () => {
		localStorage.clear()
		setUser({
			id: null,
			access_level_id: null
		})
	}

    useEffect(() => {
        unsetUser()
        history.push("/")
    },[])

    return null
}