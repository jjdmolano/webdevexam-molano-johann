import React from 'react'
import { Button } from 'react-bootstrap'

export default function DeleteUser({ setUsers, userInfo }) {
	function deleteUser(userInfo) {
		fetch(`http://localhost:4000/api/users/${userInfo._id}`, {
			method: 'DELETE',
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
			.then(res => res.json())
			.then(data => {
				fetch('http://localhost:4000/api/users', {
					method: 'GET',
					headers: {
						Authorization: `Bearer ${localStorage.getItem('token')}`
					}
				})
					.then(res => res.json())
					.then(data => {
						setUsers(data)
					})
			})
	}

	return (
		<Button variant='danger' block onClick={() => deleteUser(userInfo)}>
			Archive
		</Button>
	)
}
